const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  corePlugins: {
    preflight: false,
    opacity: false,
  },
  content: [
    './app/views/staged/view/**/*.haml',
    './lib/**/*.rb'
  ],
  plugins: [
  ]
}
