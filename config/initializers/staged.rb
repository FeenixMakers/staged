require "staged"

# directory inside /app/assets/images where YOUR images are kept
# if you change this and add own styles, you will still need a staged directory
#  for the previews (card_preview and section_preview)
Staged.images_dir = "staged"

# directory where data and styles are kept
# Notice that the data is ALWAYS inside a staged directory,
# so in the default case Rails.root/staged/*.yml
Staged.data_dir = "."

Staged.languages = ""
