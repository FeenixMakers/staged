require "opal"
require 'marked'
require 'jquery3'
require 'opal-jquery'

def switch_overlay(vis)
  overlay = Element.find("#overlay")
  overlay.css(:display ,vis)
  overlay
end
def button
  Element.find("#hidden_button")
end
def set_position(section , text)
  left = section.offset.left * 0.6
  top = section.offset.top - Document.scroll_top
  #puts "top #{top}"
  text.css(:left , "#{left}px")
  text.css(:top , "#{top}px")
end

def set_button_data( id , type)
  b = button
  b.data("id" , id)
  b.data("type" , type)
end

def set_click(clazz , target_data )
  id = target_data['id']
  target = Element.find("##{clazz}_#{id}")
  text = Element.find("#text")
  ['text' , 'header'].each do |kind|
    element = target.find(".#{clazz.downcase}_#{kind}")
    element.on(:click) do |event|
      set_position(element , text)
      set_button_data( id , clazz)
      switch_overlay( "block")
      text.find(".header").value = target_data[:header]
      text.find(".textarea").val( target_data[:text] )
    end
  end
end

def get_data(type , id )
  $$.sections.each do |section|
    if type == "Section"
      return section if section['id'] == id
    else
      section[:cards].each do | card_data |
        return card_data if card_data['id'] == id
      end
    end
  end
  raise "No data found for #{type}_#{id}"
end

def reset_html( type , id , header , text )
  target_id = "##{type}_#{id}"
  data = get_data(type , id )
  data[:header] = header
  data[:text] = text
  target = Element.find(target_id)
  target.find(".#{type.downcase}_header").html( header)
  target.find(".#{type.downcase}_text").html( $$.marked.parse(text) )
  puts "setting #{type} header: #{header}"
end

def server_put(type , id , header , text)
  data = {authenticity_token: $$.authenticity_token }
  data[:header] = header
  data[:text] = text
  HTTP.patch("/staged/#{type.downcase}s/#{id}.json" , payload: data ) do |res|
    alert "Saving failed, please reload page" unless res.ok?
  end
end

def send_data( id , type)
  text_elem = Element.find("#text")
  head = text_elem.find(".header").value
  text = text_elem.find(".textarea").val
  puts "ID #{id}:#{type}"
  reset_html( type , id , head , text)
  server_put( type , id , head , text)
  switch_overlay("none")
end

def init_section()
  $$.sections.each do |section_data|
    set_click("Section" , section_data )
    section_data[:cards].each do | card_data |
      set_click( "Card" , card_data)
    end
  end
  button.on(:click) do |event|
    b = event.target
    send_data(b.data("id") , b.data("type"))
  end
  Document.on(:keydown) do |event|
    switch_overlay("none") if (event[:key] == "Escape")
  end
end
