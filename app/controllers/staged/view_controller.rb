module Staged
  class ViewController < ::ApplicationController

    def page
      @page = Page.find_by_name(params[:id])
      redirect_to "/" , alert: "No page #{params[:id]}" unless @page
    end

  end
end
