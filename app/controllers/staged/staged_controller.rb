module Staged
  class StagedController < ::ApplicationController
    before_action :authenticate_if
    layout "staged/application"

    def authenticate_if
      return unless respond_to? :"authenticate_member!"
      authenticate_member!
    end

    def current_member_email
      return "staged_user" unless respond_to? :current_member
      current_member.email
    end
  end
end
