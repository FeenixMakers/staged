require "open3"

module Staged
  class ChangesController < StagedController

    def index
    end

    def commit
      if params[:message].blank?
        message = "must have message"
        redirect_to changes_index_url , alert: message
      else
        out = capture("git add staged")
        out = capture("git add #{Image.asset_root}" , out)
        out = capture("git config --local user.email #{current_member_email}" ,out)
        out = capture('git commit -m "' + params[:message] + '"' ,out)
        out = capture("git pull --rebase=true" ,out)
        out = capture("git push" ,out)
        ChangeSet.current.zero
        redirect_to changes_index_url , notice: "Changes commited, #{out}"
      end
    end

    def reset
      begin
        out = capture("git checkout #{Staged.data_dir}")
        out = capture("git checkout #{Staged::Image.asset_root}" , out)
        ChangeSet.current.zero
        message = "Changes reset"
      rescue
        message = "Unknown error occured"
      end
      redirect_to changes_index_url , notice: message
    end

    private
    def capture(cmd , previous = "")
      out , ignore_status = Open3.capture2e(cmd)
      puts "CMD:#{ignore_status}: #{cmd}"
      puts out
      previous + "\n" + out
    end
  end
end
