module Staged
  class CardsController < StagedController
    before_action :set_card , except: [:index , :new]

    def index
      @section = Section.find(params[:section_id])
    end

    def set_image
      @card.image_id = params[:image_id].to_i
      @card.edit_save(current_member_email)
      message = @card.image ? "#{@card.image.name} selected" : "Image removed"
      redirect_to section_cards_url(@card.section.id) , notice: message
    end

    def move
      if( params[:dir] == "up")
        @card.move_up
      else
        @card.move_down
      end
      @card.edit_save(current_member_email)
      redirect_to section_cards_url(@card.section.id),notice: "#{@card.header} moved"
    end

    def new
      @section = Section.find(params[:section_id])
      new_card =  @section.new_card
      new_card.add_save(current_member_email)
      redirect_to section_cards_url(@section.id) , notice: "Card created"
    end

    def destroy
      @card.delete_and_reset_index(current_member_email)
      redirect_to section_cards_url(@card.section.id) , notice: "#{@card.header} removed"
    end

    def update
      @card.update(params[:card])
      @card.update_options( params[:options])
      @card.edit_save(current_member_email)
      respond_to do |format|
         format.html do
           redirect_to section_cards_url(@card.section_id) , notice: "Updated #{@card.header}"
         end
         format.json { render status: 200, json: {} }
       end

    end

    private
    def set_card
      card_id = params[:id] || params[:card_id]
      @card = Card.find( card_id )
    end

  end
end
