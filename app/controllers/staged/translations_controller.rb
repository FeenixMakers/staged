module Staged
  class TranslationsController < StagedController
    before_action :set_translation, only: %i[ edit update destroy ]

    # show all translation for a page
    def show
      @page = Page.find(params[:id].to_i)
    end

    # GET /translations/new
    def new
      @translation = Translation.new
    end

    # GET /translations/1/edit
    def edit
    end

    # POST /translations
    def create
      @translation = Translation.new(translation_params)

      if @translation.save
        redirect_to @translation, notice: "Translation was successfully created."
      else
        render :new, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /translations/1
    def update
      respond_to do |format|
         @translation.update(params[:translation] , "user")
         format.html { redirect_to(@translation, :notice => 'User was successfully updated.') }
         format.json { render status: 200, json: {} }
       end
    end

    # DELETE /translations/1
    def destroy
      @translation.destroy
      redirect_to translations_url, notice: "Translation was successfully destroyed.", status: :see_other
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_translation
        @translation = Translation.find(params[:id])
      end

      # Only allow a list of trusted parameters through.
      def translation_params
        params.fetch(:translation, {})
      end
  end
end
