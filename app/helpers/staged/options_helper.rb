module Staged
  module OptionsHelper

    # use options with as many option names as neccessary
    def options(section, *args )
      all = {}
      extra_class = ""
      extra_class = args.pop if args.last.is_a?(String)
      args.each do |option_name|
        hash = send "#{option_name}_option".to_sym , section
        all.merge!(hash) { |key, one, two| one.to_s + " " + two.to_s }
      end
      all[:class] = all[:class] + " #{extra_class}"
      all
    end

    def date_precision(element , date_name)
      precision = element.option("date_precision")
      date = element.option(date_name)
      if( precision == "precise")
        return date.to_formatted_s(:short) + " " + date.year.to_s
      end
      return Date.today if date.blank?
      if(date.day < 10)
        attr = "Beginning"
      elsif date.day < 20
        attr = "Middle"
      else
        attr = "End"
      end
      "#{attr} of #{date.strftime('%B')} #{date.year}"
    end

    # adds prose irrespective of options
    def prose_option(section)
      prose_classes
    end

    def order_option(section , clazz = "")
      if section.has_option?("order")
        clazz += " order-last" if section.option('order') == "right"
      end
      {class: clazz}
    end

    def text_align_option(section , clazz = "")
      if section.has_option?("text_align")
        # text-center , text-left , text-right , leave comment for tailwind
        clazz += " text-#{section.option('text_align')}"
      end
      {class: clazz}
    end

    def item_align_option(section , clazz = "")
      if section.has_option?("item_align")
        case section.option("item_align")
        when "left"
          clazz += " " + " justify-start"
        when "right"
          clazz += " " + " justify-end"
        else
          clazz += " " + " justify-center"
        end
      end
      {class: clazz }
    end

    def background_option(section , clazz = "")
      if section.has_option?("background")
        option = section.option('background')
        clazz += " " + (Staged.background[section.option('background')] || "")
      end
      {class: clazz}
    end

    def margin_option(section , clazz = "")
      return {class: clazz} unless section.has_option?("margin")
      margin = Staged.margin[section.option("margin")] || ""
      {class: clazz + margin}
    end

    def text_color_option(section , clazz = "")
      if section.has_option?("text_color")
        clazz += " " + (Staged.text_color[section.option("text_color")] || "")
      end
      {class: clazz }
    end

    def shade_option(section , clazz = "")
      if section.has_option?("shade_color")
        clazz += " " + (Staged.shade_color[section.option("shade_color")] || "")
      end
      {class: clazz }
    end

    def text_columns_option( section , clazz = "")
      if section.has_option?("text_columns")
        clazz += " " + (Staged.text_columns[section.option("text_columns")] || "")
      end
      {class: clazz }
    end

    def slider_columns_option( section , clazz = "")
      if section.has_option?("slider_columns")
        Staged.slider_columns[section.option("slider_columns")]
      else
        Staged.slider_columns["1"]
      end
    end

    def column_option( section , clazz = "" )
      if section.has_option?("columns")
        clazz += " " + (Staged.columns[section.option("columns")] || "")
      end
      {class: clazz }
    end


  end
end
