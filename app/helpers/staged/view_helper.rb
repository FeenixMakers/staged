module Staged
  module ViewHelper
    include StagedHelper

    def current_lang
      params[:lang]
    end

    def render_section(section)
      template = "staged/view/" + section.template
      render( template , section: section)
    end

    # background image as inline style
    def bg(section , clazz = "")
      return {class: clazz} if section.image.blank?
      img = asset_url( section.image.asset_name )
      style = "background-image: url('#{img}');"
      if(section.option("fixed") == "on")
        clazz += " bg-fixed"
      end
      if(align = section.option("image_align"))
        # for tailwind: bg-left-top bg-left bg-left-bottom
        # bg-top bg-center bg-bottom bg-right-top bg-right bg-right-bottom
        clazz += " bg-#{align}"
      end
      {class: clazz , style: style}
    end

    # works for with sections and cards that respond to .image
    def image_for(element , classes = "")
      return "" if element.image.blank?
      image = element.image
      image_tag(image.asset_name , alt: image.name , class: classes )
    end

    def button_classes
      "inline-block rounded-lg px-3 py-2 text-base font-medium border border-gray-500 hover:border-black"
    end

    def view_button(element , extra_classes = "")
      return "" unless element.has_option?("button_link")
      ["<button class='#{button_classes} ",
        extra_classes,
        "'>" ,
        "<a href='" ,
        element.option("button_link") ,
        "'>" ,
        element.option("button_text") ,
        "</a>",
      " </button>"].join.html_safe
    end
    # just for tailwind bg-cyan-200

    def self.last_blog
      blog = Page.find_by_type(:blog)
      return  nil unless blog
      blog.sections.last
    end
    def last_blog
      PagesHelper.last_blog
    end

    def header_list
      return "" unless blog_section = last_blog
      blog = blog_section.page
      headers = blog.sections.collect{|s| "- " + s.header}
      headers.shift
      return "" if headers.empty?
      markdown headers.join("\n")
    end

  end
end
