module Staged
  module CardsHelper
    include ViewHelper #for previews

    def card_section_select(card)
      card_sections = card.section.page.sections
      card_sections.delete_if{|c| c.card_template.blank?}
      card_sections.collect{|s| [s.header , s.id]}
    end
  end
end
