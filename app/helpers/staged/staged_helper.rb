
module Staged
  module StagedHelper
    include OptionsHelper
    include SharedHelper

    def updated_by(owner)
      member = owner.updated_by
      return member if member.is_a? String
      return "" if member.nil?
      return member.name if member.respond_to? :name
      member.email
    end
    def aspect_ratio image
      x , y = image.aspect_ratio
      "#{x} / #{y}"
    end

    def card_field_name(card)
      name = card.header
      name += "*" unless card.option("compulsory") == "no"
      name
    end

    def last_change_digit
      last = ChangeSet.current.last
      return 10 unless last
      last = (Time.now - last).to_i
      return 10 if ( last >= 600 )
      digit =  last / 60
      digit
    end

    def last_change_class
      digit = last_change_digit
      return "" if digit > 9
      digit = 9 - digit
      reds = { "1" => "bg-red-100","2" => "bg-red-200","3" => "bg-red-100",
               "4" => "bg-red-400","5" => "bg-red-500","6" => "bg-red-600",
               "7" => "bg-red-700","8" => "bg-red-600","9" => "bg-red-900"}

      clazz = reds[digit.to_s].to_s
      clazz += " " + "text-white" if digit > 7
      clazz
    end

    def last_change_text
      digit = last_change_digit
      return "no change" if digit > 9
      "#{digit} min. by #{ChangeSet.current.last_editor}"
    end

  end
end
