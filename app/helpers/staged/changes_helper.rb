require 'open3'
module Staged
  module ChangesHelper

    def branch
      %x[git branch]
    end

    def cms_part? name
      return true if name.include?( Staged.images_dir )
      return true if name.include?(Staged.data_dir)
      false
    end

    def changeset( type , element)
      case type
      when :add
        ChangeSet.current.added( element )
      when :edit
        ChangeSet.current.edited( element )
      when :delete
        ChangeSet.current.deleted( element )
      else
        raise "unrecognized type #{type}"
      end
    end

  end
end
