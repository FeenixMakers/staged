module Staged
  module TranslationsHelper
    def language_bg lang
      case lang
      when "fi"
        "border-blue-800"
      when "sv"
        "border-yellow-400"
      else
        ""
      end
    end
  end
end
