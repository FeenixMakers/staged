module Staged
  class SectionStyle < Style

    set_root_path Rails.root.join(Staged.data_dir)

    fields  :template , :text , :header, :fields , :cards

    def has_cards?
      cards
    end

    def section_preview
      "staged/section_preview/" + template
    end

  end
end
