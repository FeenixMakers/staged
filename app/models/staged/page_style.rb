module Staged
  class PageStyle < Style
    set_root_path Rails.root.join(Staged.data_dir)

    fields  :type , :description , :section_template

    def section_preview
      "staged/section_preview/" + section_template
    end

  end
end
