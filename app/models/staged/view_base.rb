module Staged

  # base class for viewable elements: ie page, section and casrd
  # they share the template idea, options , change tracking,
  # and the fact that they persist in ActiveYaml

  class ViewBase < ActiveBase

    fields :options ,  :image_id

    def header_text(lang)
      return self.header if lang.blank?
      get_translation(lang).header
    end

    def text_text(lang)
      return self.text if lang.blank?
      get_translation(lang).text
    end

    def type_id
      "#{object_type}_#{self.id}"
    end

    def last_update_for(elements)
      last = Time.now
      last_section = nil
      elements.each do |section|
        if( section.updated_at < last )
          last = section.updated_at
          last_section = section
        end
      end
      last_section
    end

    def has_option?(option)
      safe_options.has_key?(option) and !safe_options[option].blank?
    end

    def option_definitions
      template_style.options_definitions
    end

    def option(name)
      opts = safe_options
      opts[name]
    end

    def safe_options
      return data[:options] unless data[:options].blank?
      data[:options] = {}
    end

    def set_option( option , value)
      if( option.ends_with?"_date" )
        puts "date is #{value}"
        year = value[:year] || Time.new.year
        value = Time.new( year.to_i , value[:month] , value[:day]).to_date
      end
      safe_options[option] = value
    end

    def add_default_options( definitions = nil )
      definitions = option_definitions if definitions.nil?
      definitions.each do |option|
        next unless option.default
        set_option( option.name , option.default)
      end
    end

    def update(hash)
      return unless hash
      hash.each do |key , value|
        value = value.to_i if key.to_s.include?("_id")
        key = key.to_sym
        raise "unsuported field :#{key} for #{template}:#{allowed_fields}" unless allowed_fields.include?(key)
        if(! data[key].nil? ) # first setting ok, types not (yet?) specified
          if( data[key].class != value.class )
            raise "Type mismatch #{key} #{data[key].class}!=#{value.class}"
          end
        end
        data[key] = value
      end
    end

    def update_options( options )
      return unless options
      option_definitions.each do |option|
        set_option(option.name,  options[option.name])
      end
    end
    #other may be nil
    def swap_index_with(other)
      return unless other
      old_index = self.index
      self.index = other.index
      other.index = old_index
    end

    def allowed_fields
      template_style.fields.collect{|f| f.to_sym}
    end

    def image_old
      Image.find_by_name(self.image_name)
    end

    def get_translation(lang)
      Translation.find_translation(self.id , self.object_type , lang)
    end

    def delete_translations(editor)
      Staged.language_strings.each do |lang|
        trans = Translation.find_translation(self.id, self.object_type , lang)
        trans.delete_save!(editor)
      end
    end

    def self.fix_translations
      create = []
      Staged.language_strings.each do |lang|
        self.all.each do |object|
          data = Translation.ensure_translation(object , lang)
          (create << data) unless data.nil?
        end
      end
      #puts "#{self.name} create #{create.length} Translations"
      create.each{ |data| Translation.new_translation(data) }
      Translation.save_all
    end

  end
end
