module Staged
  class Section < ViewBase

    set_root_path Rails.root.join(Staged.data_dir)

    fields :id , :page_id , :index
    fields :template , :card_template
    fields :header, :text , :image_name

    def page_name
      page ? page.name : "missing page"
    end

    def change_name
      "#{page_name}:#{header}"
    end

    def index_name
      "#{self.index}"
    end

    def object_type
      "Section"
    end

    def missing_translations
      sum = 0
      cards.collect{ |s | sum += s.missing_translations }
      Staged.language_strings.each do |lang|
        sum += 1 if get_translation(lang).is_old?
      end
      sum
    end

    def cards
      Card.find_all(:section_id, id).sort_by{|obj| obj.index }
    end

    def cards_update
      last_update_for( cards )
    end

    def template_style
      SectionStyle.find_by_template( template )
    end

    def card_template_style
      CardStyle.find_by_template( card_template )
    end

    def set_template(new_template)
      self.template = new_template
      new_style = template_style
      if(new_style.has_cards?)
        if card_template.blank?
          self.card_template = CardStyle.first.template
        end
      end
    end

    def allowed_fields
      super + [:page_id]
    end

    def has_cards?
      template_style.has_cards?
    end

    def move_up
      swap_index_with(previous_section)
    end

    def move_down
      swap_index_with(next_section)
    end

    def previous_section
      page.sections.find{|s|  s.index == index - 1 }
    end

    def next_section
      page.sections.find{|s|  s.index == index + 1 }
    end

    def new_card
      card = Card.new_card(card_template, self.id , cards.length + 1)
      card
    end

    def reset_index
      cards.each_with_index{|card, index| card.index = index + 1}
    end

    def delete(editor)
      cards.each {|card| card.delete(editor) }
      delete_translations(editor)
      delete_save!(editor)
    end

    def delete_and_reset_index(editor)
      delete(editor)
      Page.find(page_id).reset_index
      Section.save_all
    end

    def self.new_section(template , page_id , index)
      data = { template: template , index: index , page_id: page_id}
      style = SectionStyle.find_by_template( template)
      style.fields.each do |key|
        data[key] = key.upcase
      end unless style.fields.blank?
      if(style.has_cards?)
        data["card_template"] = CardStyle.first.template
      end
      new_id = Section.append(data)
      s = Section.new(data)
      s.add_default_options
      s
    end

    def self.fix_cards
      Section.all.each do |section|
        next if section.template_style.has_cards?
        puts "#{section.id}: #{section.card_template}"
        section.card_template = nil if section.card_template
      end
      Section.save_all
      nil
    end

    def self.reload
      res = super()
      self.fix_translations
    end

  end
end
