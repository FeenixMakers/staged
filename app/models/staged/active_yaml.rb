module Staged
  class ActiveYaml
    attr :data   #data of the instance, hash

    # retain the given hash data instance
    def initialize(data)
      @data = data
      # some checks, eg values are only fields
    end

    def id
      data[:id]
    end
    def id=(new_id)
      raise "Id must be number not (#{new_id})" unless new_id.is_a?(Integer)
      data[:id] = new_id
    end

    extend ActiveModel::Naming
    include ActiveModel::Conversion

    def persisted?
      true
    end

    class_attribute :path # usually rails root, where data is stored
    class_attribute :yaml_file   # the loaded data as array
    class_attribute :field_names  # methos name for accessing / allowed fields

    class << self
      #return all yaml data converted
      def all
        self.reload if self.yaml_file.blank?
        yaml_file.collect{|item| self.new(item) }
      end

      def primary_key
        "id"
      end

      # loads the data from path, name implicit in class_name
      def set_root_path(path)
        self.path = path
      end

      def reload
        self.yaml_file = load_file
      end

      def first
        self.new(yaml_file.first)
      end

      def find( id )
        id = id.to_i unless id.is_a?(Integer)
        find_by( :id , id )
      end

      def find_by( field , value )
        found = yaml_file.detect { |record| record[field] == value }
        return nil unless found
        self.new(found)
      end

      def find_all( field , value )
        found = yaml_file.find_all { |record| record[field] == value }
        found.collect {|f| self.new(f)}
      end

      def define_access(field)
        define_method(field) do
          data[field]
        end
        define_method(:"#{field}=") do |new_val|
          data[field] = new_val
        end
        assoc , id = field.to_s.split("_") # ie field image_id
        define_association(assoc) if id == "id"  #creates method image
      end

      # aka belongs_to
      # define a method by given name that used id value and
      # name of association to get a model instance of the id
      def define_association(assoc)
        clazz = "Staged::#{assoc.to_s.capitalize}".constantize
        define_method(assoc) do
          id = data[:"#{assoc}_id"]
          clazz.find( id )
        end
      end

      # class defines fields, we define access methods and finder for each
      def fields(*fields)
        self.field_names = fields
        raise "Fields must be Array not #{fields.class}" unless fields.is_a?(Array)
        fields.each do |field|
          define_access(field)
          the_meta_class.define_method(:"find_by_#{field}") do |arg|
            find_by(field , arg)
          end
        end
      end

      # load the yaml data from path and class_name
      def load_file
        YAML.load(File.read(full_path) , permitted_classes: [Time , Symbol],aliases: true)
      end

      # filename for the data (load and save)
      # made up of the set dir (set_root_path) plus tableized class name
      def full_path
        mod , filename = self.name.split("::").collect{|p| p.underscore}
        full_name = mod + "/" + filename.tableize + ".yml"
        File.join(self.path , full_name )
      end

      #create a new data point by adding the hash
      def append(data)
        largest = 0
        yaml_file.each { |item| largest = item[:id] if item[:id] > largest }
        data[:id] = largest + 1
        yaml_file << data
        largest + 1
      end

      def save_all
        File.write( full_path , yaml_file.to_yaml)
      end

      def delete(id)
        yaml_file.delete_if{|record| record[:id] == id.to_i}
        true
      end

      def the_meta_class
        class << self
          self
        end
      end

    end
  end
end
