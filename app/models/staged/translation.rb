module Staged
  class Translation < ActiveBase

    set_root_path Rails.root.join(Staged.data_dir)

    fields :id
    fields :objectid , :object_type # poly: card or section (id without _, no association)
    fields :language # the language for which the is a translation
    fields :text , :header, :sub_header #actual text fields

    def initialize(data)
      super(data)
      raise "no object type" unless self.object_type
      raise "no object id" unless lang = self.objectid
      raise "no language" unless lang = self.language
      raise "not configured language #{lang}" unless Staged.has_language(lang)
    end

    def object
      clazz = "Staged::#{object_type}".constantize
      clazz.find( objectid )
    end

    def is_old?
      return true unless self.updated_at
      object.updated_at > self.updated_at
    end

    def change_name
      "Translation: #{object.change_name}"
    end

    def update(hash , user)
      return unless hash
      [:text , :header ].each do |key|
        data[key] = hash[key] if hash.has_key?(key)
      end
      add_save(user)
    end

    class << self

      def fill_data(object, data = {})
        data[:text] = object.text
        data[:header] = object.header
        data[:objectid] = object.id
        data[:object_type] = object.object_type
        data
      end

      def find_translation(objectid , object_type, language)
        find_all(:objectid, objectid).each do |o|
          return o if (objectid == o.objectid    ) &&
                    (object_type == o.object_type) &&
                    (language == o.language)
        end
        nil
      end

      def ensure_translation(object , language)
        found = self.find_translation(object.id , object.object_type , language)
        return nil if found
        data = fill_data(object)
        data[:updated_at] = object.updated_at - 1.day
        data[:language] = language
        data[:id] = "#{object.object_type}_#{object.id}"
        data
      end

      def new_object_translation(object, data)
        self.fill_data(object,data)
        self.new_translation(data)
      end

      # append data and create new object.
      def new_translation( data )
        new_id = Translation.append(data)
        Translation.new(data)
      end

    end
  end
end
