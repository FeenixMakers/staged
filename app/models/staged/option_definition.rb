module Staged

  # Quite simple mapping from name to usually  a selection of values.
  # Options have type that can be set but usually is inferred from
  # wether there is one values (select) other wise text.
  # date is also supported.
  #
  # a corresponding ./app/views/staged/sections/_option_form_TYPE.haml
  # nust exist to render the type.
  #
  # Also values may be provided by a module function/attribute on Staged.
  # eg option text_color (also shade_color) have a Staged.text_color that return a
  # hash. In this case so it is easy to customize the colors of the app.
  #
  class OptionDefinition < SharedBase

    set_root_path Rails.root.join(Staged.data_dir)

    fields :name , :default , :description , :values , :type

    def type
      return data[:type] unless data[:type].blank?
      if has_values?
        "select"
      else
        "text"
      end
    end

    def has_values?
      return true if Staged.respond_to?( name.to_sym )
      return false if data[:values].nil?
      ! data[:values].empty?
    end

    def values
      return Staged.send(name.to_sym).keys if Staged.respond_to?( name.to_sym )
      return [] unless has_values?
      data[:values].split(" ")
    end

  end
end
