module Staged
  # shared data across engine and app
  # takes a little footwork, as seen below
  # and does not work with saving anymore.

  class SharedBase < ActiveYaml

    def self.load_file
      # does super work ? Copied, wet wet wet
      mod , filename = self.name.split("::").collect{|p| p.underscore}
      fullname = mod + "/" + filename.tableize + ".yml"
      res = YAML.load(File.read(File.join(self.path , fullname )))

      staged_path = Staged::Engine.root.join("config")
      staged_res = YAML.load(File.read(File.join(staged_path , fullname )))

      res + staged_res
    end
  end
end
