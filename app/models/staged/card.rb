module Staged

  class Card < ViewBase

    set_root_path Rails.root.join(Staged.data_dir)

    fields  :id , :index , :section_id
    fields  :text , :header, :image_name

    def change_name
      pagename = section ? section.page_name : section_id.to_s
      "#{pagename}:#{header}"
    end

    def missing_translations
      sum = 0
      Staged.language_strings.each do |lang|
        sum += 1 if get_translation(lang).is_old?
      end
      sum
    end

    def move_up
      swap_index_with(previous_card)
    end

    def move_down
      swap_index_with(next_card)
    end

    def previous_card
      section.cards.find{|card| card.index == index - 1}
    end

    def next_card
      section.cards.find{|card| card.index == index + 1}
    end

    # card templates are stored in the section so the all cards
    # have the same
    def template
      section.card_template
    end

    def template_style
      CardStyle.find_by_template( self.template )
    end

    def allowed_fields
      super + [:section_id]
    end

    def delete_and_reset_index(editor)
      delete_save!(editor)
      Section.find(section_id).reset_index
      Card.save_all
    end

    def object_type
      "Card"
    end

    def index_name
      "#{section.index}.#{self.index}"
    end

    def delete(editor)
      delete_translations(editor)
      delete_save!(editor)
    end

    def self.new_card(card_template , section_id , index)
      data = { section_id: section_id , index: index}
      template = CardStyle.find_by_template( card_template )
      template.fields.each do |key|
        data[key.to_sym] = "NEW"
      end
      new_id = Card.append(data)
      card = Card.new(data)
      card.add_default_options(template.options_definitions)
      card
    end

    def self.reload
      res = super()
      self.fix_translations
    end

  end
end
