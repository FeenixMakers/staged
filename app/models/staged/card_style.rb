module Staged
  class CardStyle < Style
    set_root_path Rails.root.join(Staged.data_dir)

    fields  :template , :text , :header, :fields

    def card_preview
      "staged/card_preview/" + template
    end

  end
end
