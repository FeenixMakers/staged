require "rails"
require "staged"

namespace :staged do

  desc "Check the data, usefull after hand edits"
  task :consistency  => :environment do
     {Section: [:image, :page] , Card: [:section , :image]}.each do |name , keys|
      Staged.check_class(name, keys)
    end
    Staged.check_images
  end

  desc "Fix missing pages"
  task :fix_pages  => :environment do
    Staged.fix_pages
  end
  desc "Fix screwed indexes"
  task :fix_indexes  => :environment do
    Staged.fix_indexes
  end
  desc "Fix missing images"
  task :fix_images  => :environment do
    Staged.fix_images
  end

end

module Staged

  def self.fix_indexes
    Page.all.each do |page|
      fix_index_for(page)
    end
    Section.save_all
  end
  def self.fix_index_for(page)
    page.sections.each_with_index do |section , index|
      next if section.index == index + 1
      puts "Section #{section.page.id}.#{section.index} == #{index + 1}"
      section.index = index + 1
    end
  end
  def self.fix_pages
    sections = Section.all.to_a.delete_if{|s| s.page}
    sections.each do |section|
      section.delete("rake fix_pages")
    end
  end

  def self.fix_images
    sections = Section.all.to_a.delete_if{|s| s.image}
    sections.each do |section|
      section.image_id = ""
      section.save
    end
  end

  def self.check_class(name , ids)
    clazz = self.const_get name.to_s  # eg Section ,  Card
    clazz.all.each do |elem|
      ids.each do |id|  # eg image, page
        check_id(elem , id )
      end
    end
  end

  def self.check_id(elem , id) # eg section instance, :image
    attr = elem.data["#{id}_id".to_sym]
    return if attr.blank?
    begin
      key = self.const_get(id.to_s.capitalize).find( attr.to_i)
    rescue
      puts "#{elem.class.name.split('::').last}:#{elem.id} missing #{id}:#{attr}"
    end
  end

  def self.check_images
    Image.all.each do |image|
      next if File.exist?(image.full_filename)
      puts "missing image #{image.full_filename}"
    end
  end
end
