module Staged
  class Converter
    def initialize
      @page_mapping = linearize Page
      @section_mapping = linearize Section
      @card_mapping = linearize Card
      @image_mapping = linearize Image
    end
    def linearize( model )
      id_mapping = {}
      count = 1
      model.all.each do |m|
        # map old to new
        old_file = m.full_filename if model == Image
        id_mapping[m.id] = count
        m.id = count
        if( model == Image && old_file != m.full_filename)
          FileUtils.mv( old_file , m.full_filename )
        end
        count = count + 1
      end
      id_mapping
    end

    def condense
      fix_sections
      fix_cards
      [Page , Image , Section , Card].each {|m| m.save_all}
    end

    def fix_images
      #empty as work is done in cards and sections
    end
    def fix_sections
      Section.all.each do |section|
        if( ! section.image_id.blank? )
          section.image_id = @image_mapping[section.image_id]
        end
        section.page_id = @page_mapping[section.page_id]
      end
    end

    def fix_cards
      Card.all.each do |card|
        if( ! card.image_id.blank? )
          card.image_id = @image_mapping[card.image_id]
        end
        card.section_id = @section_mapping[card.section_id]
      end
    end
  end
end


namespace :staged do
  desc "Make ids consecutive on all models"
  task :condense => :environment do
    Staged::Converter.new.condense
  end
end
