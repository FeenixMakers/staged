require "staged"
require "rails/generators"

module Staged
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("templates", __dir__)

    def install
      [:card_styles, :option_definitions, :page_styles , :section_styles,
      :cards, :sections , :pages, :images].each do |file|
        copy_file "empty.yml", "staged/#{file}.yml"
      end
      copy_file "initializer.rb", "config/initializers/staged.rb"
      empty_directory Staged::Image.asset_root
      empty_directory Staged.data_dir + "/staged"
    end

  end
end
