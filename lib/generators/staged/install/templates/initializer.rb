require "staged"

# directory inside /app/assets/images where YOUR images are kept
# if you change this and add own styles, you will still need a staged directory
#  for the previews (card_preview and section_preview)
Staged.images_dir = "staged"

# If languages are defined to non blank, translations can be made and
# urls for each langiage are generated, eg /fi/page_name.haml
Staged.languages = ""

# directory where data and styles are kept
# Notice that the data is ALWAYS inside a staged directory,
# so in the default case Rails.root/staged/*.yml
Staged.data_dir = "."


# text colors, keys are options shown to user, values what gets replaced
Staged.text_color =  { "white" => "text-white",
          "none" => "",
          "light_blue" => "text-cyan-100",
          "light_gray" => "text-gray-100",
          "solid_blue" => "text-cyan-700",
          "solid_red" => "text-orange-800",
          "solid_green" => "text-green-700",
          "solid_petrol" => "text-teal-700",
          "solid_indigo" => "text-indigo-800",
          "solid_black" => "text-slate-800",
        }

# margin option, keys are options shown to user, values what gets replaced
Staged.margin =  { "none" =>  "m-0",
              "small" => " m-2 md:m-4  lg:6  xl:m-8",
              "medium" => "m-5 md:m-8  lg:10 xl:m-14",
              "large" => " m-8 md:m-12 lg:16 xl:m-20",}

# background colors
Staged.background = {"white" =>  "bg-white",
                  "none" =>  "",
                  "light_blue" => "bg-cyan-100",
                  "light_gray" => "bg-gray-100",
                  "light_orange" => "bg-orange-50",
                  "solid_blue" => "bg-cyan-700  text-white",
                  "solid_red" => "bg-amber-600  text-white",
                  "solid_green" => "bg-green-700 text-white",
                  "solid_petrol" => "bg-teal-700  text-white",
                  "solid_indigo" => "bg-cyan-900 text-white",
                  "solid_black" => "bg-slate-900  text-white",  }

# shade options
Staged.shade_color =  {"white_25" => "bg-white/25",
                    "none" => "",
                    "black_25" => "bg-black/25",
                    "light_blue_25" => "bg-cyan-100/25",
                    "light_red_25" => "bg-orange-300/25",
                    "solid_blue_25" => "bg-cyan-700/25",
                    "solid_red_25" => "bg-orange-800/25", }

# amount of text columns
Staged.text_columns = {
      "3" => "columns-1 md:columns-2 lg:columns-3",
      "4" => "columns-1 md:columns-2 lg:columns-3 xl:columns-4",
      "2" => "columns-1 md:columns-2" }

Staged.columns = { "1" => "grid-cols-1",
                "2" => "grid-cols-1 md:grid-cols-2" ,
                "3" => "grid-cols-1 md:grid-cols-2 lg:grid-cols-3",
                "4" => "grid-cols-1 md:grid-cols-2 lg:grid-cols-4",
                "5" => "grid-cols-1 md:grid-cols-3 lg:grid-cols-5",
                "6" => "grid-cols-2 md:grid-cols-4 lg:grid-cols-6", }
