require 'rails/generators/base'

module Staged
  class UpdateGenerator < Rails::Generators::Base
    source_root File.expand_path("../../../../app/assets/stylesheets", __dir__)

    def update
      copy_file "staged_tailwind_styles.css", "app/assets/stylesheets/staged_tailwind_styles.css"
    end
  end
end
