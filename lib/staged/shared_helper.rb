require "redcarpet"

module Staged
  module SharedHelper

    def renderer
      options = {hard_wrap: true , autolink: true, no_intra_emphasis: true ,
          safe_links_only: true, no_styles: true ,
          link_attributes: { target: '_blank' }}
      html = Redcarpet::Render::HTML.new(options)
      Redcarpet::Markdown.new(html, options)
    end

    def prose_classes
      classes  = "prose md:prose-lg lg:prose-xl max-w-none "
      classes += "prose-headings:text-inherit "
      { class: classes }
    end

    def markdown_image(section, lang = "")
      return "" unless section.text
      down = self.renderer.render(section.text_text(lang))
      image = image_for(section)
      down.gsub("IMAGE" , image).html_safe
    end

    def markdown(text , lang = "")
      text = text.text_text(lang) unless text.is_a?(String)
      return "" if text.blank?
      self.renderer.render(text).html_safe
    end

    def rows( text )
      return 5 if text.blank?
      text = text.text unless text.is_a?(String)
      return 5 if text.blank?
      rows = (text.length / 50).to_i
      rows += text.count("\n")
      return 5 if rows < 5
      rows
    end

  end
end
