require "ruby2js"
require "ruby2js/es2015"
require "ruby2js/filter/functions"
require "ruby2js/haml"
require "ruby2js/filter/vue"
require "staged/shared_helper"
require "simple_form"
require "simple_form_tailwind_css"
require "haml-rails"
require "opal-rails"
require "opal-jquery"
require "jquery-rails"
require 'sprockets/railtie'
require "tailwindcss-rails"
require "rabl"

module Staged
  class Engine < ::Rails::Engine
    isolate_namespace Staged
    config.staged = Staged

    initializer "staged.assets.precompile" do |app|
      app.config.assets.precompile += %w( staged/staged.css staged/staged.email.css
                        staged/home staged/staged_logo staged_application.js)
      add_image_assets(app.config , "section_preview")
      add_image_assets(app.config , "card_preview")
    end

    initializer "after_initialize" do |app|
      ActiveSupport::Reloader.to_prepare do
        [Translation, Section, Card, Page, Image, PageStyle,
          SectionStyle, CardStyle, OptionDefinition].each do |clazz|
          clazz.reload
        end
      end
    end

    private
    def add_image_assets(config ,  sub_dir )
      dir = Dir.new(Engine.root.join("app/assets/images/staged/" , sub_dir))
      dir.children.each do |file|
        kid = "staged/" + sub_dir + "/" + file
        config.assets.precompile << kid
      end
    end
  end
end

module ActionDispatch::Routing
  class Mapper

    # staged_routes will draw neccessary routes and possibly mount engine
    # This includes:
    # post  /form for form processing
    # get   /news/:id for any news posts
    # get   /:id      for any pages
    # get   /:lang/:id      for every language and page
    # redirects from any renamed page
    # root to /index unless options[:root] == false
    # mount the engine (make editing possible) unless production
    #                  or oprions[:production] == true  (not recommended)
    def staged_routes options = {}
      Rails.application.routes.draw do
        begin
          if options[:root]
            root "staged/view#page" , id: 'index'
          end
          Staged.language_strings.each do |lang|
            get "/#{lang}/:id" , to: "staged/view#page" , lang: lang , id: :id
            get "/#{lang}" , to: "staged/view#page" , lang: lang , id: 'index'
          end

          Staged::Page.all.each do |page|
            next unless page.redirects
            page.redirects.split.each do |old|
              next if old == page.name
              get "/#{old}" => redirect("/#{page.name}")
            end
          end

          post "/form" , to: "staged/form#post" , as: :post_form
          get "/news/:id" , to: "staged/view#page" , id: :id , as: :view_blog
          get ":id" , to: "staged/view#page" , id: :id , as: :view_page

          engine_path = options[:path] || "/staged"
          if ! Rails.env.production? or options[:production].present?
            mount Staged::Engine => engine_path
          end

        rescue => e
          puts e.backtrace
          raise e
        end
      end
    end
  end
end
