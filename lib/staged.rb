require "staged/version"
require "staged/engine"
require "generators/staged/install/install_generator"
require "generators/staged/update/update_generator"

module Staged

  # Directory inside the app/assets/images
  mattr_accessor :images_dir
  @@images_dir = "staged"

  # Languages (space seperated) for multi-lamguage sites
  mattr_accessor :languages
  @@languages = ""

  def self.has_language( lang )
    @@languages.include?(lang)
  end

  def self.language_strings
    @@languages.split
  end

  # directory in root to store data
  mattr_accessor :data_dir
  @@data_dir = "."

  # text colors, keys are options shown to user, values what gets replaced
  mattr_accessor :text_color
  @@text_color =  { "white" => "text-white",
          "none" => "",
          "light_blue" => "text-cyan-100",
          "light_gray" => "text-gray-100",
          "solid_blue" => "text-cyan-700",
          "solid_red" => "text-orange-800",
          "solid_green" => "text-green-700",
          "solid_petrol" => "text-teal-700",
          "solid_indigo" => "text-indigo-800",
          "solid_black" => "text-slate-800",
        }

  # margin option, keys are options shown to user, values what gets replaced
  mattr_accessor :margin
  @@margin =  { "none" =>  "m-0",
              "small" => " m-2 md:m-4  lg:6  xl:m-8",
              "medium" => "m-5 md:m-8  lg:10 xl:m-14",
              "large" => " m-8 md:m-12 lg:16 xl:m-20",}

  # background colors
  mattr_accessor :background
  @@background = {"white" =>  "bg-white",
                  "none" =>  "",
                  "light_blue" => "bg-cyan-100",
                  "light_gray" => "bg-gray-100",
                  "light_orange" => "bg-orange-50",
                  "solid_blue" => "bg-cyan-700  text-white",
                  "solid_red" => "bg-amber-600  text-white",
                  "solid_green" => "bg-green-700 text-white",
                  "solid_petrol" => "bg-teal-700  text-white",
                  "solid_indigo" => "bg-cyan-900 text-white",
                  "solid_black" => "bg-slate-900  text-white",  }

  # shade options
  mattr_accessor :shade_color
  @@shade_color =  {"white_25" => "bg-white/25",
                    "none" => "",
                    "black_25" => "bg-black/25",
                    "light_blue_25" => "bg-cyan-100/25",
                    "light_red_25" => "bg-orange-300/25",
                    "solid_blue_25" => "bg-cyan-700/25",
                    "solid_red_25" => "bg-orange-800/25", }

  # amount of text columns
  mattr_accessor :text_columns
  @@text_columns = {
      "3" => "columns-1 md:columns-2 lg:columns-3",
      "4" => "columns-1 md:columns-2 lg:columns-3 xl:columns-4",
      "2" => "columns-1 md:columns-2" }

  # amount of slider columns
  mattr_accessor :slider_columns
  @@slider_columns = {
    "1" => {  640 => { slidesPerView: 1, spaceBetween: 20 }, },
    "2" => {  640 => { slidesPerView: 1, spaceBetween: 20 },
              1280 => { slidesPerView: 2, spaceBetween: 30 }, },
    "3" => {  640 => { slidesPerView: 1, spaceBetween: 20 },
              1024 => { slidesPerView: 2, spaceBetween: 30 },
              1536 => { slidesPerView: 3, spaceBetween: 40 } },
    "4" => {  640 => { slidesPerView: 1, spaceBetween: 20 },
              1024 => { slidesPerView: 2, spaceBetween: 30 },
              1536 => { slidesPerView: 4, spaceBetween: 40 }  },
   }

  # amount of text columns
  mattr_accessor :columns
  @@columns = {
    "1" => "grid-cols-1 gap-8 md:gap-12 lg:gap-16",
    "2" => "grid-cols-1 md:grid-cols-2  gap-8 md:gap-12 lg:gap-16" ,
    "3" => "grid-cols-1 md:grid-cols-2 lg:grid-cols-3  gap-8 md:gap-12 lg:gap-16",
    "4" => "grid-cols-1 md:grid-cols-2 lg:grid-cols-4  gap-8 md:gap-12 lg:gap-16",
    "5" => "grid-cols-1 md:grid-cols-3 lg:grid-cols-5  gap-8 md:gap-12 lg:gap-16",
    "6" => "grid-cols-2 md:grid-cols-4 lg:grid-cols-6  gap-8 md:gap-12 lg:gap-16", }

end
