# Staged

A CMS that integrates into the rails workflow. Ie it is file based
not db based.

Changes propagate in the normal development cycle, with git, possible
branches, possible staging, possible reviews and controlled deploys.

## Usage

Staged is designed for developers to give limited editing facilities
to users. As with rails, there is great flexibility how this can be
achieved, see wiki for more details.

## Installation

And more info see the wiki

## Contributing

Ask first.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
