require_relative "lib/staged/version"

Gem::Specification.new do |spec|
  spec.name        = "staged"
  spec.version     = Staged::VERSION
  spec.authors     = ["Torsten"]
  spec.email       = ["torsten@rubydesign.fi"]
  spec.homepage    = "https://codeberg.org/FeenixMakers/staged"
  spec.summary     = "A file based cms used in staged environment"
  spec.description = "Changes propagate in the normal development cycle, with git, possible branches, possible staging, possible reviews and controlled deploys."
  spec.license     = "MIT"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://codeberg.org/FeenixMakers/staged"
  spec.metadata["changelog_uri"] = "https://codeberg.org/HubFeenixMakers/staged"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0"
  spec.add_dependency "haml-rails"
  spec.add_dependency "redcarpet"
  spec.add_dependency "mini_magick"
  spec.add_dependency "ruby2js" ,  '~> 5.0', '>= 5.0.1'
  spec.add_dependency "simple_form_tailwind_css"
  spec.add_dependency "simple_form" , "5.1.0"
  spec.add_dependency "opal-rails" , "~> 2.0"
  spec.add_dependency 'jquery-rails'
  spec.add_dependency 'opal-jquery'
  spec.add_dependency "tailwindcss-rails"
  spec.add_dependency "rabl"

end
