require "staged"

# directory inside /app/assets/images where the images are kept
Staged.images_dir = "staged"

# directory where data and styles are kept
# Notice that the data is ALWAYS inside a staged directory,
# so in the default case Rails.root/staged/*.yml
Staged.data_dir = "."

Staged.languages = "fi sv"
