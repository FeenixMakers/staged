require "test_helper"

class SectionsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers # Rails >= 5

  def go_index
    visit "/staged/pages"
    click_on ("index")
  end
  def go_edit
    go_index
    within("#section_31") do
      find_link("Edit").click
    end
  end
  def test_edit_is
    go_index
    within("#section_31") do
      assert has_link?("Edit")
    end
  end

  def test_edit_ok
    go_index
    within("#section_31") do
      find_link("Edit").click
    end
  end

  def test_update
    go_edit
    within(".content_update") do
      find_button("Update").click
    end
  end
  class SectionsWriteTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers # Rails >= 5
    include Staged::Cleanup

    def test_remove_image
      visit "staged/sections/31"
      within(".image") do
        click_link("Remove image")
      end
      assert_text page , "No Image"
      assert_equal 200 , page.status_code
    end

  end
end
