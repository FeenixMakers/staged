require "test_helper"

class ImagesTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers # Rails >= 5

  def test_index
    visit "/staged/images"
    assert_title page, "Staged"
    assert_text page, "Pages"
  end

  def test_has_picures
    Capybara.current_driver = :selenium_headless # js: true
    expexted_num = Staged::Image.all.count
    visit "/staged/images"
    found= find_all(".image_box").count
    assert_equal found, expexted_num
  end
end
