require "test_helper"

class CardsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers # Rails >= 5

  def go_edit
    visit "/staged/sections/11"
    find_link("View and Edit Cards").click
  end
  def test_edit_is
    go_edit
    assert_equal page.current_path , "/staged/sections/11/cards"
  end

  def test_update
    go_edit
    within("#card_6") do
      find_button("Update" , match: :first).click
    end
  end

  class CardsWriteTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers # Rails >= 5
    include Staged::Cleanup

    def test_remove_image
      visit "staged/sections/11/cards"
      find_button("Remove image", match: :first).click
      assert_equal 200 , page.status_code
    end

    def test_remove_image_force
      first = Staged::Card.first.id
      visit "staged/cards/#{first}/set_image?image=''"
      assert_text page , "No image"
    end
  end
end
