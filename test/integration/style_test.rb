require "test_helper"

class StylesTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers # Rails >= 5

  def test_returns_success
    visit "/staged/styles/index"
    assert_title page, "Staged"
    assert_text page, "Styles"
  end
end
