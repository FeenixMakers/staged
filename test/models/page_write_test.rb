require "test_helper"

module Staged
  class PageWriteTests < ActiveSupport::TestCase
    include Cleanup

    def index
      Page.find_by_name('index')
    end

    def test_creates_page
      name = "randomname"
      page = Page.new_page( name , "page")
      assert_equal page.name , name
      assert_equal page.sections.length , 0
      assert page.id
    end

    def test_creates_removes_space
      name = "Cap Space"
      page = Page.new_page( name , "page")
      assert_equal page.name , "cap_space"
      assert_equal page.sections.length , 0
    end

    def test_creates_blog
      name = "randomname"
      page = Page.new_page( name , "blog")
      assert_equal page.name , name
      page.add_save("me")
      assert_equal 0 , page.sections.length
      assert page.id
    end

    def test_deletes
      id = index.id
      index.delete("you")
      assert_nil Page.find(id)
    end

    def test_destroys
      first = Page.first
      id = first.id
      first.delete("you")
      assert_nil Page.find(id)
    end

    def test_destroys_sections
      page = Page.find 2
      ids = page.sections.collect{|s| s.id}
      page.delete("you")
      Section.reload
      ids.each do | sec_id|
        assert_nil Section.find(sec_id)
      end
    end

    def test_creates_simple_section
      s = index.new_section("section_spacer")
      assert s
      assert s.id
      assert_equal s.template ,  "section_spacer"
    end

    def test_creates_section_with_right_index
      should_be = index.sections.last.index + 1
      s = index.new_section("section_spacer")
      assert_equal s.index ,  should_be
    end

  end
end
