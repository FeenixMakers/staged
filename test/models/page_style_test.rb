require "test_helper"

module Staged
  class PageStyleTest < ActiveSupport::TestCase
    def first
      PageStyle.all.first
    end

    def test_finds_stye
      spacer = PageStyle.find_by_type("page")
      assert spacer
    end

    def test_has_page_and_blog
      assert_equal 2 , PageStyle.all.length
    end

    def test_page_desc
      assert_includes  first.description , "page"
    end
  end
end
