require "test_helper"

module Staged
  class ActiveYamlTest < ActiveSupport::TestCase
    def first
      PageStyle.all.first
    end

    def test_data_loaded_is_yaml
      data = Page.load_file
      assert_equal data.class , Array
      assert_equal data.length , 2
    end

    def test_data_loaded
      assert_equal Page.all.class , Array
      assert_equal Page.all.first.class , Page
      assert_equal Page.all.length , 2
    end

    def test_fields_names
      assert Page.field_names
      assert Page.field_names.length > 4
    end

    def test_field_getter
      assert first.description
      assert first.description.length > 5
    end
    def test_field_getter
      str = "testing the description in page"
      obj = first
      obj.description = str
      assert_equal str , obj.description
    end

    def test_finds_stye
      assert PageStyle.find_by_type("page")
    end

    def test_has_sections
#      assert_equal PageStyle.all.length ,  1
    end
    def test_Spacer_has_no_fields
#      assert first.description
    end
  end
end
