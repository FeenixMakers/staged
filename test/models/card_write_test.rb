require "test_helper"

module Staged
  class CardWriteTest < ActiveSupport::TestCase
    include CardHelper
    include Cleanup

    def test_deletes
      id = first.id
      first.delete_save!("random")
      assert_nil Card.find(id)
    end

    def test_delete_index
      section = first.section
      index = first.index
      first.delete_and_reset_index("me")
      assert_equal 1 , section.cards.first.index
    end

    def test_adds
      card = Card.first.section.new_card
      card.add_save!("me me")
      assert_equal "NEW" , card.header
    end

    def test_image_set
      image = Image.first
      card = Card.first
      card.image_id = image.id
      assert_equal image.id , card.image_id
    end

    def test_deletes_translation
      card = Card.first
      assert trans = card.get_translation("fi" )
      card.delete("email")
      assert_nil card.get_translation("fi" )
    end

  end
end
