require "test_helper"

module Staged
  class PageTest < ActiveSupport::TestCase
    def first_card
      CardStyle.all.first
    end

    def test_has_all
      assert CardStyle.all.length > 5
    end

    def test_has_fields
      assert_equal first_card.field_names.class ,  Array
      assert_equal 4 , first_card.field_names.length
      assert_equal first_card.field_names.first ,  :template
    end
  end
end
