require "test_helper"

module Staged
  class TranslationTest < ActiveSupport::TestCase

    def first
      Translation.first
    end

    def test_add
      n = Translation.new_translation( objectid: 2 ,
                      object_type: "Card", text: "moi" , language: "sv" )
      assert_equal n.text , "moi"
      assert_equal 2 , n.objectid
      assert_equal "sv" , n.language
      assert n.id
    end

    def test_object
      card = Card.first
      trans = Translation.find_translation(card.id, card.object_type , "fi")
      assert trans
      c = trans.object
      assert c
      assert_equal c.id , card.id
      assert_equal c.text , card.text
    end

    def test_old
      card = Card.first
      trans = Translation.find_translation(card.id, card.object_type , "fi")
      assert trans.is_old?
      assert card.updated_at > trans.updated_at
    end

    def test_add_card
      card = Card.first
      n = Translation.new_object_translation card , language: "fi"
      assert_equal n.text , card.text
      assert_equal "fi" , n.language
      assert n.id
      n.id
    end

    def test_find_translation
      t = Translation.new_object_translation( Card.first , language: "fi")
      trans = Translation.find_translation( t.objectid , "Card" , "fi")
      assert trans
      assert_equal t.objectid , trans.objectid
      assert_equal "Card" , trans.object_type
    end

    def test_new_translation
      trans = Translation.ensure_translation( Card.all[3] , "fi")
      assert_nil trans
    end

    def test_no_lang
      assert_raises( Exception )  {Translation.new_translation}
    end

    def test_wrong_lang
      assert_raises( Exception )  {Translation.new_translation(lang: "en")}
    end

    def test_double_card
      card = Card.all[2]
      data = { text: "moi" , language: "fi"}
      n = Translation.new_object_translation( card, data)
      assert_raises(Exception){ Translation.new_card_translation(card,data) }
    end

    def test_all_sections
      Section.all.each do |s|
        assert s.get_translation("fi")
      end
    end


  end
end
