require "test_helper"

module Staged
  class CardTest < ActiveSupport::TestCase
    include CardHelper

    def test_has_all
      assert Card.all.length >= 20
    end

    def test_has_cards
      assert_equal Card , first.class
    end

    def test_has_index
      assert_equal 1 , first.index
    end

    def test_first_has_no_previous
      assert_equal first.index ,  1
      assert_nil first.previous_card
    end

    def test_first_has_next
      assert_equal first.next_card.index ,  2
    end

    def test_create_new
      card = Card.new_card("card_normal_square" , 1 , 1)
      assert_equal 1 , card.index
      assert card.id >= 47
    end

    def test_change_name
      assert_equal "studios:Standard" , first.change_name
    end

  end
end
