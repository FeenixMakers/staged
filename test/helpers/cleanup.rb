module Staged
  module Cleanup
    def teardown
      Open3.capture2e("git checkout test/dummy/staged")
      [Page, Section, Card].each { |m| m.reload }
    end
  end
end
